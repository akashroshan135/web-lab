// calling express and setting it to app
const express = require("express");
const app = express();

// view engine, renders views
app.set("view engine", "ejs");

// uses assets and css
app.use(express.static(__dirname + "/public"));

// routes
app.get("/", (req, res) => {
    res.render("index");
});
app.get("/forums", (req, res) => {
    res.render("forums");
});
app.get("/article", (req, res) => {
    res.render("article");
});
app.get("/about", (req, res) => {
    res.render("about");
});
app.get("/post", (req, res) => {
    res.render("post");
});

// app listener
app.listen(3000, () => console.log("App is running\nServer at http://localhost:3000/"));
